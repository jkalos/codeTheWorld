//Disclaimer, this is written to be run on localhost, so code will differ from my leetcode submission a tiny bit, my leetcode account is in the readme.
//Given an array of integers, return indices of the two numbers such that they add up to a specific target.
//You may assume that each input would have exactly one solution, and you may not use the same element twice.

//This article helped me so much...... https://levelup.gitconnected.com/how-to-solve-two-sum-in-javascript-d1ebd9dfd3d3
// Have to start thinking about time complexity 

let nums = [3, 2, 4];
let target = 5;

var twoSum = function(nums, target) {
    let hashTable = {}; // as per the article, an object can act as a sort of key value pair in nodejs
    let arrLen = nums.length;

    for(i = 0; i < arrLen ; i++){
        let diff = target - nums[i]; //logically the value we are looking for is the 'target' - the current place in the array, the array should have an value at a certain index == to the value
        if(hashTable[diff] != undefined && hashTable[diff] !== i){
            return [hashTable[diff], i]
        } else {
            hashTable[nums[i]] = i
        }        
    };
};

console.log(twoSum(nums, target));